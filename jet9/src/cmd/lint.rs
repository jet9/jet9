use lib_lint::Linter;
use lib_template::Template;
use structopt::StructOpt;

use git2::Repository;
use lib_config::Config;
use std::path::PathBuf;

#[derive(Debug, StructOpt)]
pub struct Lint {
  pub template: PathBuf,
}

pub fn lint_all(template: &Template, config: &Config) {
  let repo = Repository::open(".git").unwrap();
  let mut walk = repo.revwalk().unwrap();
  let linter = Linter::new(config, template);

  walk.push_head().unwrap();

  for oid in walk.into_iter() {
    let oid = oid.unwrap();
    let commit = repo.find_commit(oid).unwrap();
    let message = commit.message_bytes();
    match linter.lint(message) {
      Ok(_) => {}
      Err(issues) => {
        println!("found issues for commit #{}", oid);
        println!("```\n{}```", std::str::from_utf8(message).unwrap());
        for issue in issues {
          println!("{:#?}", issue);
        }
        println!("\n\n");
      }
    }
  }
}
