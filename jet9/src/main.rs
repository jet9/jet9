use lib_config::Config;
use lib_template::Template;
use std::fs;
use structopt::StructOpt;

mod cmd;
use cmd::{lint_all, Lint};

#[derive(Debug, StructOpt)]
enum App {
  Lint(Lint),
}

fn main() {
  let app = App::from_args();

  match &app {
    App::Lint(props) => {
      let template_source = fs::read_to_string(&props.template).unwrap();
      let template = Template::new(&template_source).expect("Could not generate template");
      let config = Config::read("jet9.yml").expect("Could not parse yml config");
      lint_all(&template, &config);
    }
  }
}
