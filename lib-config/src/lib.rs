use serde::Deserialize;
use std::fs;
use std::io::{self, Error, Result};

/// Program wide configuration file (like `jet9.config.yml`)
#[derive(Debug, Deserialize, PartialEq)]
#[serde(rename_all = "kebab-case")]
pub struct Config {
  pub allow_trailing_new_lines: bool,
}

impl Default for Config {
  fn default() -> Self {
    Self {
      allow_trailing_new_lines: true,
    }
  }
}

impl Config {
  pub fn read(path: &str) -> Result<Self> {
    match fs::canonicalize(path) {
      Ok(path) => {
        let file = fs::File::open(&path)?;

        match path
          .extension()
          .expect("config has no extension")
          .to_str()
          .unwrap()
        {
          "yml" | "yaml" => Ok(serde_yaml::from_reader(&file).unwrap_or(Self::default())),
          _ => Err(Error::new(
            io::ErrorKind::Other,
            "Unsupported config extension",
          )),
        }
      }
      Err(_) => Ok(Self::default()),
    }
  }
}
