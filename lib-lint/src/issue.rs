use lib_position::Position;

#[derive(Debug, PartialEq)]
pub enum IssueLevel {
  Error,
  Warning,
}

#[derive(Debug, PartialEq)]
pub struct UnexpectedContent<'issue> {
  pub start: Position,
  pub end: Position,
  pub actual: &'issue str,
}

#[derive(Debug, PartialEq)]
pub enum Expected<'issue> {
  Row {
    row: usize,
  },
  AnyString {
    at: Position,
  },
  String {
    at: Position,
    expected: &'issue str,
    actual: &'issue str,
  },
}

#[derive(Debug, PartialEq)]
pub enum Issue<'issue> {
  UnexpectedContent(IssueLevel, UnexpectedContent<'issue>),
  Expected(IssueLevel, Expected<'issue>),
}
