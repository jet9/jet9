use issue::{Issue, IssueLevel};
use lib_config::Config;
use lib_explorer::Explorer;
use lib_position::Position;
use lib_template::component::Component;
use lib_template::Template;

pub mod issue;

#[derive(Debug)]
pub struct Linter<'linter> {
  config: &'linter Config,
  template: &'linter Template<'linter>,
}

impl<'linter> Linter<'linter> {
  #[inline]
  pub fn new(config: &'linter Config, template: &'linter Template) -> Self {
    Self { config, template }
  }

  pub fn lint(&self, commit: &'linter [u8]) -> Result<(), Vec<Issue>> {
    let explorer = Explorer::new(commit);
    let mut issues = Vec::new();
    let mut index = 0;
    let mut cursor = Position::new(0, 0);

    loop {
      match self.template.components.get(index) {
        Some(component) => {
          index += 1;
          cursor.col += 1;

          match component {
            Component::Row(..) => {
              cursor.row += 1;
              cursor.col = 0;
            }

            Component::RowEnd => {}

            Component::String(props) => match explorer.peek_until_next_line() {
              Some(slice) => {
                match props.expect {
                  Some(expected) => {
                    let mut actual = slice.as_str().trim_end_matches(|char| char == '\n');

                    if actual != expected {
                      if actual == "\n" {
                        actual = "";
                      }
                      issues.push(Issue::Expected(
                        IssueLevel::Error,
                        issue::Expected::String {
                          at: Position::new(explorer.row(), explorer.col() + 1),
                          actual,
                          expected,
                        },
                      ));
                    }
                  }
                  None => {
                    if slice.is_eol() {
                      issues.push(Issue::Expected(
                        IssueLevel::Error,
                        issue::Expected::AnyString {
                          at: Position::new(explorer.row(), explorer.col() + slice.len()),
                        },
                      ));
                    }
                  }
                };

                explorer.advance(&slice);
              }
              None => unimplemented!(),
            },

            _ => {}
          }
        }
        None => break,
      }
    }

    if issues.is_empty() {
      Ok(())
    } else {
      Err(issues)
    }
  }
}
