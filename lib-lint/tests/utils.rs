#[macro_export]
macro_rules! test {
  (
    name: $test_name:tt,
    template: $template_string:expr,
    ok: [
      $(
        $ok_commit:literal  as $assert_ok_issue:tt
        $(,)?
      )+
    ],
    fail: [
      $(
        $fail_commit:literal as $assert_fail_issue:tt
          $(
            > $issue_type:tt $issue_level:tt
              $issue:expr
          )+
        $(,)?
      )+
    ]
    $(,)?
  ) => {

    mod $test_name {
      use lib_config::Config;
      use lib_lint::{
        issue::{self, Issue, IssueLevel},
        Linter,
      };
      use lib_position::Position;
      use lib_template::Template;

      mod fail {
        use super::*;
        use pretty_assertions::assert_eq;

        $(
          #[test]
          fn $assert_fail_issue() {
            let config = Config::default();

            let template = Template::new($template_string).unwrap();
            println!("components: {:#?}", template.components);

            let linter = Linter::new(&config, &template);

            assert_eq!(
              linter.lint($fail_commit.as_bytes()),
              Err(
                vec![
                  $(
                    Issue::$issue_type(
                      IssueLevel::$issue_level,
                      $issue
                    )
                  ),*
                ]
              )
            );
          }
        )+
      }

      mod ok {
        use super::*;
        use pretty_assertions::assert_eq;

        $(
          #[test]
          fn $assert_ok_issue() {
            let config = Config::default();

            let template = Template::new($template_string).unwrap();
            println!("components: {:#?}", template.components);

            let linter = Linter::new(&config, &template);

            assert_eq!(
              linter.lint($ok_commit.as_bytes()),
              Ok(())
            );
          }
        )+
      }
    }
  };
}
