#[macro_use]
mod utils;

test! {
  name: any_string_entire_row,
  template: r#"
    <template>
      <row>
        <string />
      </row>
    </template>
  "#,
  ok: [
    "any string!\n" as latin_string,
    "любая строка!\n" as utf8_string,
  ],
  fail: [
    "\n" as empty_string
      > Expected Error
        issue::Expected::AnyString {
          at: Position::new(1, 1),
        }
  ]
}

test! {
  name: specific_string_entire_row,
  template: r#"
    <template>
      <row>
        <string expect="green!" />
      </row>
    </template>
  "#,
  ok: [
    "green!\n" as exact_match_should_work,
  ],
  fail: [
    "\n" as empty_string
      > Expected Error
        issue::Expected::String {
          at: Position::new(1, 1),
          actual: "",
          expected: "green!",
        }

    "not the thing...\n" as not_the_string
      > Expected Error
        issue::Expected::String {
          at: Position::new(1, 1),
          actual: "not the thing...",
          expected: "green!",
        }
  ]
}
