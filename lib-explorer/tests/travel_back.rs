use lib_explorer::Explorer;
use pretty_assertions::assert_eq;

#[test]
fn travel_back_from_the_initial_slice() {
  let explorer = Explorer::new("one two три".as_bytes());

  let slice = explorer.peek_until_next_line().unwrap();

  let t = slice.travel_back(1).unwrap();
  assert_eq!(t, explorer.peek(10).unwrap());

  let t = slice.travel_back(3).unwrap();
  assert_eq!(t, explorer.peek(8).unwrap());

  let t = slice.travel_back(10).unwrap();
  assert_eq!(t, explorer.peek(1).unwrap());
}

#[test]
fn travel_back_from_the_subslice() {
  let explorer = Explorer::new("one two три".as_bytes());

  let slice = explorer.peek_until_next_line().unwrap();

  let t = slice.travel_back(1).unwrap();
  assert_eq!(t, explorer.peek(10).unwrap());

  let t = t.travel_back(1).unwrap();
  assert_eq!(t, explorer.peek(9).unwrap());

  let t = t.travel_back(3).unwrap();
  assert_eq!(t, explorer.peek(6).unwrap());

  let t = t.travel_back(5).unwrap();
  assert_eq!(t, explorer.peek(1).unwrap());
}

#[test]
fn should_not_be_able_to_travel_past_the_start() {
  let explorer = Explorer::new("один".as_bytes());

  let slice = explorer.peek(4).unwrap();

  assert_eq!(slice.travel_back(4), None);
  assert_eq!(slice.travel_back(5), None);
  assert_eq!(slice.travel_back(10), None);
}

#[test]
fn travel_back_accounting_for_the_changing_lines() {
  let explorer = Explorer::new("one\nдва\nthree\n".as_bytes());

  let slice = explorer.peek(14).unwrap();

  let t = slice.travel_back(1).unwrap();
  assert_eq!(t, explorer.peek(13).unwrap());

  let t = t.travel_back(8).unwrap();
  assert_eq!(t, explorer.peek(5).unwrap());

  let t = t.travel_back(3).unwrap();
  assert_eq!(t, explorer.peek(2).unwrap());
}
