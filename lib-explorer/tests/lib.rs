use lib_explorer::Explorer;

mod ascii {
  use super::*;
  use pretty_assertions::assert_eq;

  #[test]
  fn has_more() {
    let explorer = Explorer::new("hey".as_bytes());

    assert_eq!(explorer.take(1).unwrap().as_str(), "h");
    assert_eq!(explorer.col(), 1);

    assert_eq!(explorer.has_more(), true);

    assert_eq!(explorer.take_until_next_line().unwrap().as_str(), "ey");
    assert_eq!(explorer.col(), 3);

    assert_eq!(explorer.has_more(), false);

    assert_eq!(explorer.take(1), None);
    assert_eq!(explorer.col(), 3);

    assert_eq!(explorer.has_more(), false);
  }
}

mod utf8 {
  use super::*;
  use pretty_assertions::assert_eq;

  #[test]
  fn has_more() {
    let explorer = Explorer::new("хой".as_bytes());

    assert_eq!(explorer.take(1).unwrap().as_str(), "х");
    assert_eq!(explorer.col(), 1);

    assert_eq!(explorer.has_more(), true);

    assert_eq!(explorer.take_until_next_line().unwrap().as_str(), "ой");
    assert_eq!(explorer.col(), 3);

    assert_eq!(explorer.has_more(), false);

    assert_eq!(explorer.take(1), None);
    assert_eq!(explorer.col(), 3);

    assert_eq!(explorer.has_more(), false);
  }
}
