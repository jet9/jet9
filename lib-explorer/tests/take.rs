use lib_explorer::Explorer;
use pretty_assertions::assert_eq;

#[test]
fn should_take_one_by_one_until_the_end() {
  let explorer = Explorer::new("hey друг".as_bytes());

  assert_eq!(explorer.take(1).unwrap().as_str(), "h");
  assert_eq!(explorer.col(), 1);

  assert_eq!(explorer.take(1).unwrap().as_str(), "e");
  assert_eq!(explorer.col(), 2);

  assert_eq!(explorer.take(1).unwrap().as_str(), "y");
  assert_eq!(explorer.col(), 3);

  assert_eq!(explorer.take(1).unwrap().as_str(), " ");
  assert_eq!(explorer.col(), 4);

  assert_eq!(explorer.take(1).unwrap().as_str(), "д");
  assert_eq!(explorer.col(), 5);

  assert_eq!(explorer.take(1).unwrap().as_str(), "р");
  assert_eq!(explorer.col(), 6);

  assert_eq!(explorer.take(1).unwrap().as_str(), "у");
  assert_eq!(explorer.col(), 7);

  assert_eq!(explorer.take(1).unwrap().as_str(), "г");
  assert_eq!(explorer.col(), 8);

  assert_eq!(explorer.take(1), None);
  assert_eq!(explorer.col(), 8);
}

#[test]
fn take_three_to_match_the_input_length() {
  let explorer = Explorer::new("hey друг".as_bytes());

  assert_eq!(explorer.take(8).unwrap().as_str(), "hey друг");
  assert_eq!(explorer.col(), 8);

  assert_eq!(explorer.take(1), None);
  assert_eq!(explorer.col(), 8);
}

#[test]
fn take_as_much_as_possible() {
  let explorer = Explorer::new("hey друг".as_bytes());

  assert_eq!(explorer.take(100).unwrap().as_str(), "hey друг");
  assert_eq!(explorer.col(), 8);

  assert_eq!(explorer.take(1), None);
  assert_eq!(explorer.col(), 8);
}

#[test]
fn take_multiline() {
  let explorer = Explorer::new("Раз Два\n\nThree Four\nFive Six\n".as_bytes());

  assert_eq!(explorer.take(1).unwrap().as_str(), "Р");
  assert_eq!(explorer.row(), 1);
  assert_eq!(explorer.col(), 1);

  assert_eq!(explorer.take(2).unwrap().as_str(), "аз");
  assert_eq!(explorer.row(), 1);
  assert_eq!(explorer.col(), 3);

  assert_eq!(explorer.take(1).unwrap().as_str(), " ");
  assert_eq!(explorer.row(), 1);
  assert_eq!(explorer.col(), 4);

  assert_eq!(explorer.take(3).unwrap().as_str(), "Два");
  assert_eq!(explorer.row(), 1);
  assert_eq!(explorer.col(), 7);

  assert_eq!(explorer.take(1).unwrap().as_str(), "\n");
  assert_eq!(explorer.row(), 2);
  assert_eq!(explorer.col(), 0);

  assert_eq!(explorer.take(1).unwrap().as_str(), "\n");
  assert_eq!(explorer.row(), 3);
  assert_eq!(explorer.col(), 0);

  assert_eq!(
    explorer.take_until_next_line().unwrap().as_str(),
    "Three Four\n"
  );
  assert_eq!(explorer.row(), 4);
  assert_eq!(explorer.col(), 0);

  assert_eq!(explorer.take(4).unwrap().as_str(), "Five");
  assert_eq!(explorer.row(), 4);
  assert_eq!(explorer.col(), 4);

  assert_eq!(explorer.take(1).unwrap().as_str(), " ");
  assert_eq!(explorer.row(), 4);
  assert_eq!(explorer.col(), 5);

  assert_eq!(explorer.take_until_next_line().unwrap().as_str(), "Six\n");
  assert_eq!(explorer.row(), 5);
  assert_eq!(explorer.col(), 0);

  assert_eq!(explorer.take(1), None);
  assert_eq!(explorer.row(), 5);
  assert_eq!(explorer.col(), 0);

  assert_eq!(explorer.take_until_next_line(), None);
  assert_eq!(explorer.row(), 5);
  assert_eq!(explorer.col(), 0);
}
