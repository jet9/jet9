use lib_explorer::Explorer;
use pretty_assertions::assert_eq;

#[test]
fn no_matter_how_hard_you_peek_you_should_not_move() {
  let explorer = Explorer::new("hey друг".as_bytes());

  for _ in 1..100 {
    let val = explorer.peek(1).unwrap();
    assert_eq!(val.as_str(), "h");
    assert_eq!(val.len(), 1);
    assert_eq!(explorer.row(), 1);
    assert_eq!(explorer.col(), 0);

    let val = explorer.peek(3).unwrap();
    assert_eq!(val.as_str(), "hey");
    assert_eq!(val.len(), 3);
    assert_eq!(explorer.row(), 1);
    assert_eq!(explorer.col(), 0);

    let val = explorer.peek(100).unwrap();
    assert_eq!(val.as_str(), "hey друг");
    assert_eq!(val.len(), 8);
    assert_eq!(explorer.row(), 1);
    assert_eq!(explorer.col(), 0);
  }
}

#[test]
fn take_then_peek() {
  let explorer = Explorer::new("one\n два".as_bytes());

  explorer.take_until_next_line().unwrap();
  explorer.take(1).unwrap();

  for _ in 1..100 {
    let val = explorer.peek(1).unwrap();
    assert_eq!(val.as_str(), "д");
    assert_eq!(val.len(), 1);
    assert_eq!(explorer.row(), 2);
    assert_eq!(explorer.col(), 1);

    let val = explorer.peek(3).unwrap();
    assert_eq!(val.as_str(), "два");
    assert_eq!(val.len(), 3);
    assert_eq!(explorer.row(), 2);
    assert_eq!(explorer.col(), 1);

    let val = explorer.peek(100).unwrap();
    assert_eq!(val.as_str(), "два");
    assert_eq!(val.len(), 3);
    assert_eq!(explorer.row(), 2);
    assert_eq!(explorer.col(), 1);
  }
}
