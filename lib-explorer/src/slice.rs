use super::NEW_LINE;
use lib_position::Position;

#[derive(Debug, PartialEq)]
pub struct ExplorerSlice<'e> {
  pub bytes: &'e [u8],
  pub start: Position,
  pub end: Position,
}

impl<'e> ExplorerSlice<'e> {
  #[inline]
  pub fn as_str<'f>(&self) -> &'f str {
    unsafe { &*(self.bytes as *const [u8] as *const str) }
  }

  pub fn len(&self) -> usize {
    let mut index = 0;
    let mut len = 0;

    loop {
      match self.bytes.get(index) {
        Some(byte) => {
          len += 1;
          index += utf8_width::get_width(*byte);
        }
        None => return len,
      }
    }
  }

  #[inline]
  pub fn is_eol(&self) -> bool {
    *self.bytes.first().unwrap() == NEW_LINE
  }

  /// Get a sub-slice of the current slice.
  pub fn travel_back(&self, n_chars: usize) -> Option<ExplorerSlice> {
    let total_chars = self.len();
    let (mut steps, is_overflow) = total_chars.overflowing_sub(n_chars);

    if steps == 0 || is_overflow {
      return None;
    }

    let mut index = 0;
    let mut end = self.start;

    while steps > 0 {
      // SAFETY: by definition we are taking a subslice, which means that
      // we will not even try to touch anything that is out of bounds.
      let byte = *unsafe { self.bytes.get_unchecked(index) };

      index += utf8_width::get_width(byte);
      steps -= 1;

      match byte {
        NEW_LINE => {
          end.row += 1;
          end.col = 0;
        }
        _ => {
          end.col += 1;
        }
      }
    }

    Some(ExplorerSlice {
      bytes: unsafe { self.bytes.get_unchecked(0..index) },
      start: self.start,
      end,
    })
  }
}
