mod slice;
use lib_position::Position;
use slice::ExplorerSlice;

pub const NEW_LINE: u8 = 10;

#[derive(Debug, PartialEq)]
pub struct Explorer<'e> {
  haystack: &'e [u8],

  /// `needle` is a current index into the `haystack`.
  ///
  /// When used with ascii compatible texts it is expected to be consistent
  /// with your typical index into an array, e.g. 0th is the first element,
  /// 1st is the second, etc, however with utf8 texts it's not the case: instead, it will
  /// have a value of the current position into the haystack with an account of the utf8 char
  /// widths of the text before.
  needle: usize,

  position: Position,
}

impl<'e> Explorer<'e> {
  pub fn new(input: &'e [u8]) -> Self {
    Self {
      haystack: input,
      needle: 0,
      position: Position::default(),
    }
  }

  #[inline]
  pub fn row(&self) -> usize {
    self.position.row
  }

  #[inline]
  pub fn col(&self) -> usize {
    self.position.col
  }

  pub fn has_more(&self) -> bool {
    self.needle < self.haystack.len()
  }

  pub fn take(&self, n_chars: usize) -> Option<ExplorerSlice> {
    let slice = self.peek(n_chars);
    match slice {
      Some(slice) => {
        self.advance(&slice);
        Some(slice)
      }
      None => None,
    }
  }

  pub fn take_until_next_line(&self) -> Option<ExplorerSlice> {
    let slice = self.peek_until_next_line();
    match slice {
      Some(slice) => {
        self.advance(&slice);
        Some(slice)
      }
      None => None,
    }
  }

  #[inline]
  pub fn advance(&self, slice: &ExplorerSlice) {
    // SAFETY:
    unsafe {
      *(&self.needle as *const usize as *mut usize) += slice.bytes.len();
      self.position.merge_mut(&slice.end);
    };
  }

  pub fn peek_until_next_line(&self) -> Option<ExplorerSlice> {
    let (n_bytes, end) = {
      let mut index = self.needle;
      let mut end = self.position;

      loop {
        match self.haystack.get(index) {
          Some(byte) => {
            let char_width = utf8_width::get_width(*byte);
            index += char_width;

            match *byte {
              NEW_LINE => {
                end.row += 1;
                end.col = 0;
                break;
              }
              _ => {
                end.col += 1;
              }
            }
          }
          None => break,
        };
      }

      (index - self.needle, end)
    };

    if n_bytes == 0 {
      return None;
    }

    let lower_limit = self.needle;
    let upper_limit = lower_limit + n_bytes;

    Some(ExplorerSlice {
      bytes: unsafe { self.haystack.get_unchecked(lower_limit..upper_limit) },
      start: self.position,
      end,
    })
  }

  pub fn peek(&self, n_chars: usize) -> Option<ExplorerSlice> {
    let (n_bytes, end) = {
      let mut index = self.needle;
      let mut limit = n_chars;
      let mut end = self.position;

      while limit > 0 {
        match self.haystack.get(index) {
          Some(byte) => {
            let char_width = utf8_width::get_width(*byte);
            index += char_width;
            limit -= 1;

            match *byte {
              NEW_LINE => {
                end.row += 1;
                end.col = 0;
              }
              _ => {
                end.col += 1;
              }
            }
          }
          None => break,
        };
      }

      (index - self.needle, end)
    };

    if n_bytes == 0 {
      return None;
    }

    let lower_limit = self.needle;
    let upper_limit = lower_limit + n_bytes;

    Some(ExplorerSlice {
      bytes: unsafe { self.haystack.get_unchecked(lower_limit..upper_limit) },
      start: self.position,
      end,
    })
  }
}
