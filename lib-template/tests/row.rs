use component::{Component, Row};
use lib_template::{component, Template};
use pretty_assertions::assert_eq;

#[test]
fn empty_row() {
  let code = r###"
    <template>
      <row></row>
    </template>
  "###;

  assert_eq!(
    Template::new(&code),
    Ok(Template {
      components: vec![Component::Row(Row::default()), Component::RowEnd]
    })
  );
}

#[test]
fn empty_row_short_tag() {
  let code = r###"
    <template>
      <row />
    </template>
  "###;

  assert_eq!(
    Template::new(&code),
    Ok(Template {
      components: vec![Component::EmptyRow]
    })
  );
}

#[test]
fn max_len_for_row() {
  let code = r###"
    <template>
      <row max-len="100"></row>
    </template>
  "###;

  assert_eq!(
    Template::new(&code),
    Ok(Template {
      components: vec![
        Component::Row(Row { max_len: Some(100) }),
        Component::RowEnd,
      ]
    })
  );
}
