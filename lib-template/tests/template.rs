use lib_template::Template;
use pretty_assertions::assert_eq;

#[test]
fn empty_template_no_rules() {
  let code = r###"
    <template></template>
  "###;

  assert_eq!(Template::new(&code), Ok(Template { components: vec![] }));
}
