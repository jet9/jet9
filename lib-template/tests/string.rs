use component::{Component, Row, String};
use lib_template::{component, Template};
use pretty_assertions::assert_eq;

#[test]
fn any_string_entire_row() {
  let code = r###"
      <template>
        <row>
          <string />
        </row>
      </template>
    "###;

  assert_eq!(
    Template::new(&code),
    Ok(Template {
      components: vec![
        Component::Row(Row::default()),
        Component::String(String::default()),
        Component::RowEnd,
      ]
    })
  );
}

#[test]
fn specific_string_entire_row() {
  let code = r###"
      <template>
        <row>
          <string expect="one two three" />
        </row>
      </template>
    "###;

  assert_eq!(
    Template::new(&code),
    Ok(Template {
      components: vec![
        Component::Row(Row::default()),
        Component::String(String {
          expect: Some("one two three")
        }),
        Component::RowEnd,
      ]
    })
  );
}
