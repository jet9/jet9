#[macro_use]
extern crate pest_derive;

mod parser;

pub mod component;

mod template;
pub use template::Template;
