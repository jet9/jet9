use crate::component::{Row, String};
use crate::{component, parser};
use component::Component;

#[derive(Debug, PartialEq)]
pub struct Template<'template> {
  pub components: Vec<Component<'template>>,
}

impl<'template> Template<'template> {
  pub fn new(code: &'template str) -> Result<Self, ()> {
    let ast = parser::parse(code).expect("Could not parse template");
    let mut it = Self {
      components: Vec::with_capacity(512),
    };

    for pair in ast {
      match pair.as_rule() {
        parser::RawRule::ShortTag => {
          let mut props = pair.into_inner();
          let tag = props.next().unwrap().as_str();

          match tag {
            "row" => {
              it.components.push(Component::EmptyRow);
            }
            "string" => {
              let mut string_props = String::default();

              for attr in props {
                match attr.as_rule() {
                  parser::RawRule::TagAttr => {
                    let mut attr_props = attr.into_inner();
                    let attr_key = attr_props.next().unwrap();

                    match attr_key.as_str() {
                      "expect" => {
                        let attr_value = attr_props.next().unwrap();
                        let attr_value_inner = attr_value.into_inner().next().unwrap();
                        let value = attr_value_inner.as_str();

                        string_props.expect = Some(value);
                      }
                      _ => {}
                    }
                  }
                  _ => {}
                }
              }

              it.components.push(Component::String(string_props));
            }
            _ => {}
          }
        }
        parser::RawRule::CloseTag => {
          let mut props = pair.into_inner();
          let tag_name = props.next().unwrap();

          match tag_name.as_str() {
            "string" => unimplemented!(),
            "row" => {
              it.components.push(Component::RowEnd);
            }
            _ => {}
          }
        }
        parser::RawRule::OpenTag => {
          let mut props = pair.into_inner();
          let tag_name = props.next().unwrap();

          match tag_name.as_str() {
            "string" => unimplemented!(),
            "row" => {
              let mut row_props = Row::default();

              for attr in props {
                match attr.as_rule() {
                  parser::RawRule::TagAttr => {
                    let mut attr_props = attr.into_inner();
                    let attr_key = attr_props.next().unwrap();

                    match attr_key.as_str() {
                      "max-len" => {
                        let attr_value = attr_props.next().unwrap();
                        let attr_value_inner = attr_value.into_inner().next().unwrap();
                        let max_len = attr_value_inner.as_str().parse().unwrap();

                        row_props.max_len = Some(max_len);
                      }
                      _ => {}
                    }
                  }
                  _ => {}
                }
              }

              it.components.push(Component::Row(row_props));
            }
            _ => {}
          }
        }
        _ => {}
      }
    }

    Ok(it)
  }
}
