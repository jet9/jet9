#[derive(Debug, PartialEq)]
pub struct String<'component> {
  pub expect: Option<&'component str>,
}

impl<'component> Default for String<'component> {
  fn default() -> Self {
    Self { expect: None }
  }
}
