mod row;
mod string;

pub use row::Row;
pub use string::String;

#[derive(Debug, PartialEq)]
pub enum Component<'component> {
  Row(Row),
  RowEnd,
  EmptyRow,

  String(String<'component>),

  None,
}
