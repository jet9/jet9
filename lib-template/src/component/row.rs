#[derive(Debug, PartialEq)]
pub struct Row {
  pub max_len: Option<usize>,
}

impl Default for Row {
  fn default() -> Self {
    Self { max_len: None }
  }
}
