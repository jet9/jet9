use pest::error::Error;
use pest::iterators::Pairs;
use pest::Parser;

#[derive(Parser)]
#[grammar = "parser/grammar.pest"]
struct RawParser;

pub(crate) type RawRule = Rule;

pub(crate) fn parse(input: &str) -> Result<Pairs<RawRule>, Error<RawRule>> {
  RawParser::parse(RawRule::Program, input)
}
