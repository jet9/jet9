#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Position {
  pub row: usize,
  pub col: usize,
}

impl Position {
  #[inline]
  pub fn new(row: usize, col: usize) -> Self {
    Self { row, col }
  }

  #[inline]
  pub fn merge(&mut self, other: &Position) {
    self.row = other.row;
    self.col = other.col;
  }

  #[inline]
  pub unsafe fn set_row_mut(&self, val: usize) {
    *(&self.row as *const usize as *mut usize) = val;
  }

  #[inline]
  pub unsafe fn set_col_mut(&self, val: usize) {
    *(&self.col as *const usize as *mut usize) = val;
  }

  #[inline]
  pub unsafe fn add_row_mut(&self, row: usize) {
    self.set_row_mut(self.row + row)
  }

  #[inline]
  pub unsafe fn add_col_mut(&self, col: usize) {
    self.set_col_mut(self.col + col)
  }

  #[inline]
  pub unsafe fn merge_mut(&self, other: &Position) {
    self.set_col_mut(other.col);
    self.set_row_mut(other.row);
  }
}

impl Default for Position {
  fn default() -> Self {
    Self { row: 1, col: 0 }
  }
}

impl std::ops::Add for Position {
  type Output = Position;

  fn add(self, other: Position) -> Position {
    Position {
      row: self.row + other.row,
      col: self.col + other.col,
    }
  }
}

impl std::ops::Add for &Position {
  type Output = Position;

  fn add(self, other: &Position) -> Position {
    Position {
      row: self.row + other.row,
      col: self.col + other.col,
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use pretty_assertions::assert_eq;

  #[test]
  fn a01() {
    let a = Position::new(1, 10);
    let b = Position::new(2, 30);
    assert_eq!(a + b, Position::new(3, 40));
  }

  #[test]
  fn a02() {
    let a = Position::new(1, 10);
    let b = Position::new(2, 30);
    assert_eq!(&a + &b, Position::new(3, 40));
  }

  #[test]
  fn a03() {
    let a = Position::new(1, 10);
    let b = Position::new(2, 30);
    unsafe { a.merge_mut(&b) };
    assert_eq!(a, Position::new(2, 30));
  }

  #[test]
  fn a04() {
    let a = Position::new(1, 10);
    unsafe { a.add_row_mut(1) };
    assert_eq!(a, Position::new(2, 10));
  }

  #[test]
  fn a05() {
    let a = Position::new(1, 10);
    unsafe { a.add_col_mut(10) };
    assert_eq!(a, Position::new(1, 20));
  }
}
